const mongoose = require('mongoose');

const Note = mongoose.model('Note', {
  userId: {
    type: String,
    ref: 'User',
  },
  completed: {
    type: Boolean,
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  createdDate: {
    type: Date,
    required: true,
  },
});

module.exports = {
  Note,
};
