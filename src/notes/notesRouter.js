const express = require('express');

const {
  addNote,
  getNoteById,
  changeNote,
  checkNote,
  deleteNote,
  getNotes,
} = require('./notesService');

const router = new express.Router();

router.post('/', addNote);

router.get('/', getNotes);

router.get('/:id', getNoteById);

router.put('/:id', changeNote);

router.patch('/:id', checkNote);

router.delete('/:id', deleteNote);

module.exports = {
  notesRouter: router,
};
