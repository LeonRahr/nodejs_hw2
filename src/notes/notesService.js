
const {Note} = require('../models/Notes');
const {User} = require('../models/Users.js');

const getNotes = async (req, res, next) => {
  try {
    const {offset, limit} = req.query;
    const userId = req.user.userId;
    const notes = await Note.find({userId}).skip(+offset).limit(+limit);
    return res.json({
      offset,
      limit,
      count: notes.length,
      notes,
    });
  } catch (e) {
    console.log(e);
    return res.status(400).json({message: 'string'});
  }
};

const addNote = async (req, res, next) => {
  const {text} = req.body;
  try {
    const currentUser = await User.findById(req.user.userId);
    const note = new Note({
      text,
      userId: currentUser._id,
      completed: false,
      createdDate: new Date(),
    });

    note.save()
        .then(() => res.json({message: 'Success'}))
        .catch((err) => {
          next(err);
        });
  } catch (e) {
    console.log(e);
    return res.status(400).json({message: 'string'});
  }
};

const getNoteById = async (req, res, next) => {
  const id = req.params.id;
  try {
    const note = await Note.findById(id);
    return res.json({note});
  } catch (e) {
    console.log(e);
    return res.status(400).json({message: 'string'});
  }
};

const changeNote = async (req, res, next) => {
  const id = req.params.id;
  const {text} = req.body;
  try {
    await Note.findByIdAndUpdate(id, {text}, {new: true});
    return res.json({message: 'Success'});
  } catch (e) {
    console.log(e);
    return res.status(400).json({message: 'string'});
  }
};

const checkNote = async (req, res, next) => {
  const id = req.params.id;
  try {
    const note = await Note.findById(id);
    note.completed = !note.completed;
    note.save()
        .then(() => res.json({message: 'Success'}))
        .catch((err) => {
          next(err);
        });
  } catch (e) {
    console.log(e);
    return res.status(400).json({message: 'string'});
  }
};

const deleteNote = async (req, res, next) => {
  const id = req.params.id;
  try {
    Note.findByIdAndDelete(id)
        .then(() => res.json({message: 'Success'}))
        .catch((err) => {
          next(err);
        });
  } catch (e) {
    console.log(e);
    return res.status(400).json({message: 'string'});
  }
};

module.exports = {
  addNote,
  getNoteById,
  changeNote,
  checkNote,
  deleteNote,
  getNotes,
};
