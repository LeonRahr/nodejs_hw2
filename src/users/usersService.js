const bcrypt = require('bcryptjs');
const {User} = require('../models/Users.js');

const getUserInfo = async (req, res, next) => {
  const id = req.user.userId;
  try {
    const user = await User.findById(id);
    return res.json({
      user: {
        _id: user._id,
        username: user.username,
        createdDate: user.createdDate,
      },
    });
  } catch (e) {
    console.log(e);
    return res.status(400).message({message: 'string'});
  }
};

const changePassword = async (req, res, next) => {
  const id = req.user.userId;
  if (!req.body.newPassword) {
    return res.status(400).json({message: 'string'});
  }
  const password = await bcrypt.hash(req.body.newPassword, 10);
  try {
    await User.findByIdAndUpdate(id, {password}, {new: true});
    return res.json({message: 'Success'});
  } catch (e) {
    console.log(e);
    return res.status(400).json({message: 'string'});
  }
};

const deleteUser = async (req, res, next) => {
  const id = req.user.userId;
  try {
    User.findByIdAndDelete(id)
        .then(() => res.json({message: 'Success'}))
        .catch((err) => {
          next(err);
        });
  } catch (e) {
    console.log(e);
    return res.status(400).json({message: 'string'});
  }
};

module.exports = {
  getUserInfo,
  changePassword,
  deleteUser,
};
