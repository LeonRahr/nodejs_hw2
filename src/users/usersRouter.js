const express = require('express');

const router = new express.Router();
const {
  getUserInfo,
  changePassword,
  deleteUser,
} = require('./usersService.js');

router.get('/me', getUserInfo);

router.patch('/me', changePassword);

router.delete('/me', deleteUser);

module.exports = {
  usersRouter: router,
};
