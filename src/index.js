const fs = require('fs');
const express = require('express');
const morgan = require('morgan');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://admin:admin@leonavramenkocluster.f86ta2l.mongodb.net/?retryWrites=true&w=majority');

const {usersRouter} = require('./users/usersRouter.js');
const {authMiddleware} = require('./middleware/authMiddleware');
const {notesRouter} = require('./notes/notesRouter');
const {authRouter} = require('./auth/authRouter');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', authMiddleware, usersRouter);
app.use('/api/notes', authMiddleware, notesRouter);

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

app.use(errorHandler);
/**
  *log server error
 *@param {string} err for Error
 * @param {function} req for request
 * @param {function} res for response
 * @param {function} next for run next middleware
 */
function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({message: 'Server error'});
}
